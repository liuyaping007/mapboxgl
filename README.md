## 环境要求

node16以上版本


## 安装依赖
```
npm install
```

### 本地调试运行
```
npm run serve
```

### 打包编译
```
npm run build
```

### 效果图

3D白膜效果
![输入图片说明](public/3Dbaimo.png)


结合Echart 展示饼图统计
![输入图片说明](public/echartbingtu.png)

结合Echart 路径规划导航
![输入图片说明](public/echartdaohang.png)

结合Echart 散点图统计
![输入图片说明](public/echartshandian.png)

结合Echart 高清卫星 结合常用地图的小例子
![输入图片说明](public/gaoqingweixin.png)


结合Echart 高清卫星 结合常用地图的小例子
![输入图片说明](public/gaoqingweixin.png)

3D沉浸式规划导航
<video width="320" height="240" controls>
  <source src="public/f0c75f0810518ff9a7b9c1daf6ef4e9a.mp4" type="video/mp4">
  <source src="public/f0c75f0810518ff9a7b9c1daf6ef4e9a.mp4" type="video/ogg">
  Your browser does not support the video tag.
</video>

## 联系作者

需要其它城市的3D白膜效果的geojson  可以联系作者，也可以入群讨论。

- QQ群 109434403
- qq联系人：329175905
微信号： 18670793619
扫一扫加好友

| 微信                                                              | qq                                                                    | qq群                                                                    |
|------------------------------------------------------------------|------------------------------------------------------------------------|------------------------------------------------------------------------|
|<img src="https://oscimg.oschina.net/oscnet/up-582fc3f0b0152879dffdc0e9302b85e8a95.png" width="300" height="180px"  alt="微信"/><br/> | ![](https://oscimg.oschina.net/oscnet/up-933f0fcf9c91152aa625b06ee857f11d3f2.png) | ![](https://oscimg.oschina.net/oscnet/up-3688b49bd44df9757c3c6162d36b1f15a69.png) |
