import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Home',
        component: () => import('@/components/home.vue'),
    },
    {
        path: '/mapindex',
        name: 'mapindex',
        component: () => import('@/components/mapindex.vue'),
    },
    {
        path: '/tablegrid',
        name: 'tablegrid',
        component: () => import('@/components/tablegrid.vue'),
    }, {
        path: '/mapboxgl4',
        name: 'mapboxgl4',
        component: () => import('@/components/mapboxgl4.vue'),
    }, {
        path: '/home',
        name: 'home',
        component: () => import('@/components/home.vue'),
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

export default router
