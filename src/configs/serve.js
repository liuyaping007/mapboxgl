export default {
    apiUrl: "/api",
    title: "简搭云管理系统",
    logo: "/logo.png",
    author: "简搭云科技通用管理系统快速开发框架",
    isdevtool: false,
    versiontype: "form",
    webUrl: 'http://localhost:8083/',
    apiwebUrl: 'http://localhost:8080',
    isdemoWeb: true,
};