import Vue from 'vue'
import App from './App.vue'
import router from './router'


// import "/scss/common.scss" // 全局样式
// import "@/scss/translation.scss" // 全局css动画

import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
Vue.use(ElementUI);

Vue.config.productionTip = false


new Vue({
    router,
    render: h => h(App),
}).$mount('#app')
