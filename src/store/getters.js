const getters = {
  dragObjects: state => state.map.dragObjects,
  selectObject: state => state.map.selectObject
}
export default getters
