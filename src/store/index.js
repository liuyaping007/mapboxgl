import Vue from 'vue'
import Vuex from 'vuex'
import getters from './getters'

Vue.use(Vuex)

const modules = require.context('./modules', true, /\.js$/)
const indexList = modules.keys().filter(item => item.indexOf('index.js') !== -1)
export default new Vuex.Store({
  getters,
  modules: Object.assign({}, ...indexList.map(key => ({
    [key.replace(/(^\.\/|\/index\.js$)/g, '')]: modules(key).default
  })))
})
