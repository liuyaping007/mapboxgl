// import { Map } from "@plugin/Three-dutu";
import { DutuJS } from "@/plugin/DutuJS/index";
import { mounted } from './MapLoad'
/** map放进state会导致报错，因此放全局 */
let map = null
// 需要动态显示隐藏模型的名称数组
let dynamicShow = []
/** 实时渲染 */
// let jiaodu = 0
let render = () => {
    map.render();
    // jiaodu += 0.1
    // if (jiaodu === 89) {
    //   jiaodu += 2
    // }
    // if (jiaodu > 360) {
    //   jiaodu = 0
    // }
    // map.setRange(jiaodu)
    requestAnimationFrame(render)
}

export const actions = {
    /** 初始化 */
    mapInit (store, obj) {
        /** 场景实例化 */
        map = new DutuJS(obj);
        /** 实时渲染 */
        render()
        /** map需要加载的内容 */
        mounted(map, dynamicShow, store)
    },
    /** 显示某一个模型 */
    openItem ({ commit }, names) {
        commit('SET_SHOW_NAMES', names)
        if (names) {
            let t
            const load = () => {
                if (map) {
                    for (let i = 0; i < dynamicShow.length; i++) {
                        const item = dynamicShow[i];
                        let object = map.otherGroup.getObjectByName(item)
                        if (object) {
                            object.visible = false
                            object.scale.x = 0 // css3DRender仅设置visble不会隐藏
                        }
                    }
                    for (let j = 0; j < names.length; j++) {
                        const name = names[j];
                        let showObject = map.otherGroup.getObjectByName(name)
                        if (showObject) {
                            showObject.visible = true
                            showObject.scale.x = 1
                        }
                    }
                    clearInterval(t)
                } else {
                    // 这里是判断map有没有初始化完成，等初始化完成再进行操作
                    t = setInterval(() => {
                        load()
                    }, 1000);
                }
            }
            load()
        }
    },
    /** 隐藏某一些模型，并显示一些模型 */
    hiddenItem (store, { showNames, hiddenNames }) {
        let t
        const load = () => {
            if (map) {
                for (let i = 0; i < showNames.length; i++) {
                    const name = showNames[i];
                    let hiddenObject = map.scene.getObjectByName(name)
                    if (hiddenObject) {
                        hiddenObject.visible = true
                        hiddenObject.scale.x = 1 // css3DRender仅设置visble不会隐藏
                    }
                }
                for (let i = 0; i < hiddenNames.length; i++) {
                    const name = hiddenNames[i];
                    let hiddenObject = map.scene.getObjectByName(name)
                    if (hiddenObject) {
                        hiddenObject.visible = false
                        hiddenObject.scale.x = 0 // css3DRender仅设置visble不会隐藏
                    }
                }
                clearInterval(t)
            } else {
                // 这里是判断map有没有初始化完成，等初始化完成再进行操作
                t = setInterval(() => {
                    load()
                }, 1000);
            }
        }
        load()
    },
    /** 相机飞行 */
    cameraFly (store, { position, controlsPosition }) {
        let t
        const load = () => {
            if (map) {
                map.flyTo({ position, duration: 600, controlsPosition })
                clearInterval(t)
            } else {
                // 这里是判断map有没有初始化完成，等初始化完成再进行操作
                t = setInterval(() => {
                    load()
                }, 1000);
            }
        }
        load()

    },
    /** 新增POI标点 */
    addPoi (store, config) {
        if (map) {
            map.addCss3d(config)
        }
    },
    /** 加载obj，主要用来加载线条数据 */
    addObj (store, { fileName }) {
        return new Promise((resolve) => {
            map.addObj({
                fileName, name: fileName, beforeAdd: (obj) => {
                    resolve(obj)
                }
            })
        })
    },
    /** 加载区域2 */
    addElectronicFence (store, config) {
        map.addEleFen(config)
    },
    /** 加载飞线图 */
    addFlyLine (store, { lineObjs, name }) {
        let list = lineObjs.map(item => {
            return map.att2Points(item.geometry.attributes.position.array)
        })
        let lineList = list.map(item => {
            let obj = {
                startPoint: item[0],
                endPoint: item[1],
            }
            return obj
        })
        const height = 500
        map.addFlyLine({ lineList, height, name })
        return new Promise(resolve => {
            resolve(list)
        })
    },
    /** 更新dynamicShow数组 */
    uploadNames (store, { names }) {
        dynamicShow = dynamicShow.concat(names.filter(v => !dynamicShow.includes(v)))
    },
    /** 路网初始化 */
    roadInit (store, config) {
        map.roadInit(config)
    },
    /** 材质替换成线框 */
    toWireframe (state, config) {
        map.toWireframe(config)
    },
    roam (state, config) {
        map.roam(config)
    },
    closeRoam () {
        if (map) {
            map.closeRoam()
        }
    },
    pointMove (store, config) {
        if (map) {
            map.pointMove(config)
        }
    },
    addTubeWar (store, config) {
        if (map) {
            map.addTubeWar(config)
        }
    },
    addCollect (store, config) {
        if (map) {
            map.addCollect(config)
        }
    },
    movePoint (store, config) {
        if (map) {
            map.movePoint(config)
        }
    },
    dragObject ({ commit }, config) {
        if (map) {
            config.beforeAdd = (mesh) => {
                commit('PUSH_DRAGOBJECTS', mesh)
                return true
            }
            map.dragObject(config)
        }
    },
    dragOpen (store, config = {}) {
        if (map) {
            map.dragOpen(config)
        }
    },
    dragClose (store, config = {}) {
        if (map) {
            map.dragClose(config)
        }
    },
    changeObject ({ commit }, id) {
        if (map) {
            let object = map.attachObjectById(id)
            commit('SET_SELECTOBJECT', object)
        }
    }
}