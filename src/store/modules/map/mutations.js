export const mutations = {
  SET_SHOW_NAMES (state, names) {
    state.showNames = names
  },
  PUSH_DRAGOBJECTS (state, mesh) {
    let object2data = (mesh) => {
      let newChildren = []
      if (mesh.children && mesh.children.length > 0) {
        for (let i = 0; i < mesh.children.length; i++) {
          const element = mesh.children[i];
          newChildren.push(object2data(element))
        }
      }
      return {
        id: mesh.id,
        label: mesh.type,
        children: newChildren
      }
    }
    state.dragObjects.push(object2data(mesh))
  },
  SET_SELECTOBJECT (state, object) {
    state.selectObject = object
  }
}