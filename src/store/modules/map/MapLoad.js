export const mounted = (map, dynamicShow, store) => {
  // 加载党政管理的飞线图
  // addFlyLine(map, dynamicShow, store)
  // 加载大场景
  // addScene(map, dynamicShow, store)
  // 加载范围提示
  // addRange(map, dynamicShow, store)
}
// 加载党政管理的飞线图
const addFlyLine = () => { }
// 加载大场景
const addScene = (map) => {
  map.addGltf({
    openShadow: false, fileName: 'scene',
    beforeAdd: (mesh) => {
      setTimeout(() => {
        map.addGltf({
          openShadow: false,
          fileName: 'scene-jjs',
          beforeAdd: (mesh1, animations) => {
            map.addAni({ mesh: mesh1, animations, index: 0 })
            let reset = ['conveyer_belt__']
            for (let i = 0; i < reset.length; i++) {
              const name = reset[i];
              let meshList = mesh1.children.filter(item => item.name.indexOf(name) !== -1)
              if (meshList && meshList.length > 0) {
                meshList[0].name = name
              }
            }
            setTimeout(() => {
              const config = {
                meshName: 'conveyer_belt__',
                offsetY: -0.002
              }
              map.meshUvMove(config)
            }, 3000);
            return true
          }
        })
      }, 5000);
      let riverList = mesh.children.filter(item => item.name.indexOf('River_') !== -1)
      map.addWater(riverList[0])
      /** fbx转换成glb时会将名字后加一个数字，用此方法还原 */
      let reset = ['attic_floor__', 'first_floor__', 'seccend_floor__', 'third_floor__', 'tube_road__', 'tube_three__', 'room_main__']
      for (let i = 0; i < reset.length; i++) {
        const name = reset[i];
        let meshList = mesh.children.filter(item => item.name.indexOf(name) !== -1)
        if (meshList && meshList.length > 0) {
          meshList[0].name = name
        }
      }
      return true
    }
  })
}

const addRange = (map) => { 
  map.addSky()
  map.addCloud()
  // map.addRain()
  map.glowInit()
  map.addPlane()
  // map.dragOpen()
}