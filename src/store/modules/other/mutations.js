export const mutations = {
  SET_VIDEO_OPEN: (state, isOpen) => {
    state.videoOpen = isOpen
  },
  SET_INFO_WINDOW_15_INDEX: (state, index) => {
    state.infoWindow15Index = index
  }
}