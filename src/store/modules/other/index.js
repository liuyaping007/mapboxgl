import { state } from './state'
import { actions } from './actions.js'
import { mutations } from './mutations'

const other = {
  state,
  actions,
  mutations
}
export default other
