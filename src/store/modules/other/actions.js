let jsq = null
export let actions = {
  openVideoWindow ({ commit }, fileName) {
    commit('SET_VIDEO_OPEN', fileName)
  },
  setInfoWindow15Index ({ commit }, { index, length }) {
    commit('SET_INFO_WINDOW_15_INDEX', index)
    clearInterval(jsq)
    jsq = null
    jsq = setInterval(() => {
      if (Number(index) === Number(length) + 1) {
        index = 0
      } else {
        index++
      }
      commit('SET_INFO_WINDOW_15_INDEX', index)
    }, 10000);
  }
}
